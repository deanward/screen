var sorter = require('../sorter'),
    assert = require('assert'),
    fs =  require('fs'),
     _  =  require('underscore')


var jobdata = fs.readFileSync('jobs.json')
var jobs = JSON.parse(jobdata)


_.each(jobs, ( job ) => {
  it('Should output: ' + job.expect, function() {
    assert.equal(sorter.tasksort(job), job.expect);
  });
}); 