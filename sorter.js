var _  =  require('underscore')

const DEPENDENCY_SPLIT = ' => '

module.exports = {

  tasksort: function ( job ) {
    var parsed_dependencies = {}
    var dependency_count = job.dependencies.length;
    

    _.each(job.dependencies, ( dependency ) => {

      var parsed = dependency.split(DEPENDENCY_SPLIT)
      parsed_dependencies[parsed[0]] = parsed[1]

      var first_index = job.tasks.indexOf( parsed[0] )
      var second_index = job.tasks.indexOf( parsed[1] )

      if( second_index > first_index ) {
        job.tasks.splice(first_index, 0, job.tasks.splice(second_index, 1)[0])
      }
    })

    var seen = {}
    var cyclic = false;
    _.each(parsed_dependencies, (dep,index) => {
      if(dep in seen) {
        cyclic = true;
      }
      seen[index] = true;
    });

    if(cyclic) {
      return 'Error - this is a cyclic dependency';
    }
    
    return job.tasks.join(',');
  }

}